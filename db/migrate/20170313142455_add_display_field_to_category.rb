class AddDisplayFieldToCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :display_priority, :integer, default: 0
  end
end
