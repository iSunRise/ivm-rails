class CreateFurnitures < ActiveRecord::Migration[5.0]
  def change
    create_table :furnitures do |t|
      t.string :title
      t.text :description
      t.integer :category_id, index: true
      t.integer :subcategory_id, index: true

      t.timestamps
    end
  end
end
