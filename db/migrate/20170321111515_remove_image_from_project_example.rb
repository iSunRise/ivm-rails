class RemoveImageFromProjectExample < ActiveRecord::Migration[5.0]
  def change
    remove_column :portfolio, :image, :string
  end
end
