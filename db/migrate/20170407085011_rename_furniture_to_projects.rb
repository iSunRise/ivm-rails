class RenameFurnitureToProjects < ActiveRecord::Migration[5.0]
  def up
    rename_table :furnitures, :projects
  end

  def down
    rename_table :projects, :furnitures
  end
end
