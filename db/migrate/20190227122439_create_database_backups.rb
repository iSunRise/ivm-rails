class CreateDatabaseBackups < ActiveRecord::Migration[5.0]
  def change
    create_table :database_backups do |t|
      t.string :file
      t.datetime :processed_at

      t.timestamps null: false
    end
  end
end
