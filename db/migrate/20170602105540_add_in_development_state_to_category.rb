class AddInDevelopmentStateToCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :in_development, :boolean, default: true
  end
end
