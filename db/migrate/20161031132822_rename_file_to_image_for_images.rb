class RenameFileToImageForImages < ActiveRecord::Migration[5.0]
  def change
    rename_column :images, :file, :image
  end
end
