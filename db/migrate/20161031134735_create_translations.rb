class CreateTranslations < ActiveRecord::Migration[5.0]
  def change
    create_table :translations do |t|
      t.integer :admin_user_id
      t.string :key, index: true
      t.string :language, index: true
      t.text :content

      t.timestamps
    end
  end
end
