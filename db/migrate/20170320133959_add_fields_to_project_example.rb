class AddFieldsToProjectExample < ActiveRecord::Migration[5.0]
  def up
    remove_column :portfolio, :category, :string

    add_column :images, :project_example_id,    :integer

    add_column :portfolio, :category_id, :integer
    add_column :portfolio, :subcategory_id, :integer
    add_column :portfolio, :type_id, :integer
    add_column :portfolio, :description, :string
    add_column :portfolio, :creation_date, :date
  end

  def down
    add_column :portfolio, :category, :string

    remove_column :images, :project_example_id,    :integer

    remove_column :portfolio, :category_id, :integer
    remove_column :portfolio, :subcategory_id, :integer
    remove_column :portfolio, :type_id, :integer
    remove_column :portfolio, :description, :string
    remove_column :portfolio, :creation_date, :date
  end
end
