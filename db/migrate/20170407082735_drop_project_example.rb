class DropProjectExample < ActiveRecord::Migration[5.0]
  def up
    drop_table :portfolio if ActiveRecord::Base.connection.table_exists? 'portfolio'
  end

  def down
    create_table :portfolio
  end
end
