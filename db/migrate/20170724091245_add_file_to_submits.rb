class AddFileToSubmits < ActiveRecord::Migration[5.0]
  def change
    add_column :submits, :file, :string
  end
end
