class CreateSubcategories < ActiveRecord::Migration[5.0]
  def change
    create_table :subcategories do |t|
      t.integer :category_id, index: true
      t.string :title
      t.string :image, array: true

      t.timestamps
    end
  end
end
