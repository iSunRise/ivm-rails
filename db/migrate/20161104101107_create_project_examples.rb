class CreateProjectExamples < ActiveRecord::Migration[5.0]
  def change
    create_table :portfolio do |t|
      t.string :title
      t.string :image
      t.string :category

      t.timestamps
    end
  end
end
