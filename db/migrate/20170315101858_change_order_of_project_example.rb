class ChangeOrderOfProjectExample < ActiveRecord::Migration[5.0]
  def change
    remove_column :portfolio, :order, :integer
    add_column :portfolio, :order, :integer, default: 0
  end
end
