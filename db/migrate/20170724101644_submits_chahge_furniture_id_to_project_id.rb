class SubmitsChahgeFurnitureIdToProjectId < ActiveRecord::Migration[5.0]
  def change
    remove_column :submits, :furniture_id, :integer
    add_column    :submits, :project_id,   :integer
  end
end
