class AddFurnitureIdToReviews < ActiveRecord::Migration[5.0]
  def change
    add_column :reviews, :furniture_id, :integer
    add_column :reviews, :displayable,  :boolean, default: false
  end
end
