class CreateLastProject < ActiveRecord::Migration[5.0]
  def change
    create_table :last_projects do |t|
      t.string :img_slider

      t.timestamps
    end
  end
end
