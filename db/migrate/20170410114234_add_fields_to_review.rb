class AddFieldsToReview < ActiveRecord::Migration[5.0]
  def change
    add_column :reviews, :images, :string, default: ''
  end
end
