class DropLastProject < ActiveRecord::Migration[5.0]
  def up
    drop_table :last_projects
  end

  def down
    create_table :last_projects do |t|
      t.string :img_slider

      t.timestamps
    end
  end
end
