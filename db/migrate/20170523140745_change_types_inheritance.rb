class ChangeTypesInheritance < ActiveRecord::Migration[5.0]
  def change
    add_column :types, :category_id, :integer
    # Type.all.each do |t|
    #   t.category_id = Subcategory.find(t.subcategory_id).category.id
    #   t.save!
    # end
    remove_column :types, :subcategory_id, :integer
  end
end
