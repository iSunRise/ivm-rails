class AddFieldsToTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :types, :description,    :text
    add_column :types, :subcategory_id, :integer
  end
end
