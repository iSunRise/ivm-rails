class AddFieldsToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :creation_date, :date
    add_column :projects, :type,          :string, index: true, default: 'Furniture'
  end
end
