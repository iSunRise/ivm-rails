class AddDescriptionToSubcategories < ActiveRecord::Migration[5.0]
  def change
    add_column :subcategories, :description, :text
  end
end
