class FixSubcategoryImageField < ActiveRecord::Migration[5.0]
  def change
    remove_column :subcategories, :image, :string, array: true
    add_column :subcategories, :image, :string
  end
end
