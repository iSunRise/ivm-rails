class CreateSubmits < ActiveRecord::Migration[5.0]
  def change
    create_table :submits do |t|
      t.string  :name
      t.string  :phone
      t.integer :submit_type, default: 0
      t.jsonb   :data, default: {}
      t.integer :furniture_id

      t.timestamps
    end
  end
end
