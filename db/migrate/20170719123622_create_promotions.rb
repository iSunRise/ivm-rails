class CreatePromotions < ActiveRecord::Migration[5.0]
  def change
    create_table :promotions do |t|
      t.string  :image
      t.text    :description
      t.integer :category_id, null: false

      t.timestamps
    end

    add_foreign_key :promotions, :categories
  end
end
