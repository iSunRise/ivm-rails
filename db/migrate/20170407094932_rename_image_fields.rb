class RenameImageFields < ActiveRecord::Migration[5.0]
  def up
    add_column :images, :project_id, :integer

    Image.all.each do |img|
      if img.furniture_id.present?
        img.project_id = Project.find(img.furniture_id).id
        img.save
      else
        img.destroy!
      end
    end

    remove_column :images, :furniture_id, :integer
    remove_column :images, :project_example_id, :integer
  end

  def down
    add_column :images, :furniture_id, :integer
    add_column :images, :project_example_id, :integer

    Image.all.each do |img|
      if Furniture.find(img.project_id).present?
        img.furniture_id = img.project_id
      else
        img.destroy!
      end
    end

    remove_column :images, :project_id, :integer
  end
end
