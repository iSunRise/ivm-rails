class ChangeReviewBelongingToPortfolio < ActiveRecord::Migration[5.0]
  def change
    remove_column :reviews, :furniture_id,   :integer
    add_column    :reviews, :portfolio_id,   :integer
    add_column    :reviews, :category_id,    :integer
    add_column    :reviews, :subcategory_id, :integer
  end
end
