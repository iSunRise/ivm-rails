Rails.application.routes.draw do
  devise_for :users, ActiveAdmin::Devise.config
  devise_for :admin_users, ActiveAdmin::Devise.config

  ActiveAdmin.routes(self)

  root  'static_pages#home'

  get '/home',     to: 'static_pages#home'
  get '/kitchens', to: 'static_pages#kitchens'
  get '/ourworks', to: 'static_pages#ourworks'
  get '/livingbaby', to: 'static_pages#livingbaby'
  get '/officefurniture', to: 'static_pages#officefurniture'
  get '/wardrobes', to: 'static_pages#wardrobes'
  get '/cloakroom', to: 'static_pages#cloakroom'
  get '/hallways', to: 'static_pages#hallways'

  match 'submit', to: 'submits#cors', via: :options
  match 'submit', to: 'submits#create', via: [:get, :post]

  resources :translations, only: :create
  resources :reviews, only: [:index, :new, :create]

  get ":category_slug", to: 'categories#show'
  get ":category_slug/:subcategory_slug", to: 'subcategories#show'
end
