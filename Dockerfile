FROM ruby:2.4.2-stretch

RUN curl -sL https://deb.nodesource.com/setup_11.x | bash -
RUN apt-get install -y nodejs cron

# setup periodical backups
RUN (crontab -l ; echo "* * * 0 0 /usr/local/bundle/bin/rails runner 'BackupCreator.new.proceed'") | crontab

RUN mkdir /app
WORKDIR /app
COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock
RUN bundle install

COPY . /app
RUN bundle exec rails assets:precompile
