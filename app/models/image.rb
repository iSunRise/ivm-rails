class Image < ApplicationRecord
  belongs_to :project

  mount_uploader :image, ImageUploader

  private

  # this method should use only to recreate versions of images
  def self.recreate_all_versions
    self.all.each do |image|
      image.image.recreate_versions!
      image.save
    end
  end
end
