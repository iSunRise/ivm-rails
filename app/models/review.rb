class Review < ApplicationRecord
  mount_uploader  :avatar, AvatarUploader

  mount_uploaders :images, ImageUploader
  serialize :images, JSON

  belongs_to :portfolio

  validates_presence_of :name, :message, :portfolio
end