class Category < ApplicationRecord
  extend FriendlyId

  has_many :subcategories
  has_many :types
  has_one  :promotion, dependent: :destroy

  validates :image, presence: true

  mount_uploader :image, ImageUploader

  friendly_id :slug_candidates, use: :slugged

  def normalize_friendly_id(input)
    input.to_s.to_slug.normalize(transliterations: :russian).to_s
  end

  def slug_candidates
    [:title, :id]
  end

  def should_generate_new_friendly_id?
    title_changed?
  end
end
