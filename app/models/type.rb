class Type < ApplicationRecord
  belongs_to :category
  has_many   :furnitures

  validates_presence_of :name
end