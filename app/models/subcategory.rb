class Subcategory < ApplicationRecord
  extend FriendlyId

  belongs_to :category
  has_many   :furniture
  has_many   :portfolios

  mount_uploader :image, ImageUploader

  validates :category, presence: true

  friendly_id :slug_candidates, use: :slugged

  def normalize_friendly_id(input)
    input.to_s.to_slug.normalize(transliterations: :russian).to_s
  end

  def slug_candidates
    [:title, :id]
  end

  def should_generate_new_friendly_id?
    title_changed?
  end
end
