class DatabaseBackup < ActiveRecord::Base
  validates :file, :processed_at, presence: true
  mount_uploader :file, BackupUploader
end
