class Submit < ApplicationRecord

  mount_uploader :file, ImageUploader

  enum submit_type: [:common, :callback, :metering, :calculation, :application]

  belongs_to :project
end