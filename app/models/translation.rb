class Translation < ApplicationRecord
  after_save :clear_rails_cache
  after_destroy :clear_rails_cache

  validates :key, :language, presence: true
  validates :key, uniqueness: { scope: :language }

  def self.find_text(key)
    Rails.cache.fetch("#{key}_#{I18n.locale}") do
      Translation.find_by(key: key, language: I18n.locale).try :content
    end
  end

  def clear_rails_cache
    Rails.cache.delete("#{key}_#{I18n.locale}")
  end
end
