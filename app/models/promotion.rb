class Promotion < ApplicationRecord
  validates :category_id, presence: true, uniqueness: true

  belongs_to :category

  mount_uploader :image, ImageUploader

end