class Project < ApplicationRecord
  belongs_to :category
  belongs_to :subcategory
  belongs_to :type

  has_many   :images, dependent: :destroy

  validates :category, :subcategory, :type, presence: true

  before_save :set_date

  private

  def set_date
    self.creation_date = created_at if creation_date.blank?
  end
end