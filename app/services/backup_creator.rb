class BackupCreator
  def proceed
    # dump mysql database to file
    filepath = Dir.mktmpdir + "/#{Time.zone.now.strftime('%d-%m-%Y--%H')}.sql.gz"
    # mysql version
    # success = system("mysqldump -u #{db_user} -p#{db_pass} #{db_name} --single-transaction | gzip > #{filepath}")
    # postgres version

    database_url = "postgress://#{db_user}:#{db_pass}@#{db_host}"
    success = system("pg_dump #{ENV['DATABASE_URL'] || database_url} | gzip > #{filepath}")

    if !success
      notify_failure
      return
    end
    # upload backup to S3
    backup = DatabaseBackup.new(
      processed_at: Time.zone.now,
      file: File.open(filepath)
    )
    backup.save!
    # remove backup file
    `rm #{filepath}`
  end

  private

  def notify_failure
    Rails.logger.info('Could not dump database!!!')
  end

  def db_host
    database_configuration['host']
  end

  def db_name
    database_configuration['database']
  end

  def db_pass
    database_configuration['password']
  end

  def db_user
    database_configuration['username']
  end

  def database_configuration
    @database_configuration ||= Rails.configuration.database_configuration[Rails.env]
  end
end
