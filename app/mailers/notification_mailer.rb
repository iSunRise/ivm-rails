class NotificationMailer < ApplicationMailer

  def new_request(submit_id)
    @submit = Submit.find(submit_id)

    mail(to: ENV['ADMIN_EMAIL'], subject: "У вас нова заявка #{I18n.t('submit_types.' + @submit.submit_type.to_s)}")
  end
end
