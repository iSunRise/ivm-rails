ActiveAdmin.register Submit do
  controller do
    actions :all, :except => [:edit, :update, :create, :new]
  end

  # View
  index do
    selectable_column
    column :created_at
    column :phone
    column :name
    column ("Тип") { |sub| I18n.t('submit_types.' + sub.submit_type) }
    column :project
    actions
  end

  filter :project
  filter :submit_type,
         as: :select,
         collection: Submit.submit_types.map{ |k, v| [I18n.t('submit_types.' + k), v] }.to_h
  filter :created_at
  filter :phone

  show do
    attributes_table do
      row :id
      row :name
      row :phone
      row ("Тип") { |sub| I18n.t('submit_types.' + sub.submit_type) }
      row :data do |sub|
        sub.data.map do |k, v|
          "<p><strong>#{I18n.t('submit_data.' + k.to_s).capitalize}:</strong><br> #{v}</p><br><br>".html_safe
        end
      end
      row :created_at
      row :file
      row :project
    end
  end

end