ActiveAdmin.register Portfolio do

  # allows_multi_upload(mounted_uploader: :image)

  menu priority: 2

  permit_params :title, :description, :category_id, :subcategory_id, :type_id, image_ids: []

  filter :category, label: 'Категорія'
  filter :subcategory, label: 'Підкатегорія'
  filter :title, label: 'Назва'
  filter :description, label: 'Опис'
  filter :created_at, label: 'Створено'
  filter :updated_at, label: 'Оновлено'

  # Index
  index do
    column ('Зображення'){ |f| image_tag(f.images.first.image_url, style: 'max-width: 100px') if f.images.first }
    column 'Назва', :title
    column 'Створено', :created_at
    column 'Категорія', :category
    column 'Підкатегорія', :subcategory
    actions
  end

  # View
  show do
    attributes_table do
      row :title
      row :category
      row :subcategory
      row :description
      row ('Зображення') { |x|
        next '' if x.images.size == 0
        html = '<img src="'
        html += x.images.map {|p| p.image_url() }
                  .join('">&nbsp;&nbsp;&nbsp;<img src="')
        html += '">'
        # html += x.images.map {|p| p.image_url(:medium) }
        #                   .join('">&nbsp;&nbsp;&nbsp;<img src="')
        # html += '">'
        html.html_safe
      }
    end
  end

  # Edit
  form do |f|
    f.inputs 'Деталі' do
      f.input :type_id, as: :nested_select,
              fields: [:title, :name],
              display_name: :title,
              minimum_input_length: 0,
              level_1: { attribute: :category_id    },
              level_2: { attribute: :subcategory_id },
              level_3: {
                attribute:    :type_id,
                display_name: :name,
                url:          '/admin/furnitures/get_types'
              }

      f.input :title, input_html: { class: 'simple-input' }, include_blank: false
      f.input :description, input_html: { class: 'simple-input' }
      f.input :creation_date, input_html: { class: 'simple-input' }
    end
    f.inputs 'Зображення' do
      render 'active_admin_multi_upload/upload_form',
             resource: @resource,
             association: 'images',
             attribute: 'image',
             options: {}
    end
    f.actions
  end
end
