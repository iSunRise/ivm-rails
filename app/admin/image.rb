ActiveAdmin.register Image do

  menu false

  permit_params :image, :furniture_id, :project_example_id
  allows_multi_upload(mounted_uploader: :image)

  # View
  index do
    selectable_column
    column :id
    column :created_at
    column ('Картинка'){ |p| image_tag(p.image_url, style: 'width:100px;')}
    actions
  end

  form do |f|
    f.inputs do
      f.input :furniture
      f.input :project_example
      f.input :image
    end
    f.actions
  end

end
