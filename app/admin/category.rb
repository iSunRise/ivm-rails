ActiveAdmin.register Category do

  menu parent: "Каталог", priority: 3

  permit_params :title, :image, :slug, :display_priority, :in_development

  before_filter :only => [:show, :edit, :update, :destroy] do
    @category = Category.friendly.find(params[:id])
  end

  index do
    selectable_column
    id_column
    column :title
    column (:image){ |category| image_tag(category.image_url, style: 'width: 100px') }
    column :display_priority
    column :in_development
    actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row 'Подкатегории' do
        subc = Subcategory.where(category_id: resource.id)
        # add a links to subcategory with ', ' separation if this is not last item
        subc.map do |subcategory|
          link_to subcategory.title, admin_subcategory_path(subcategory.id)
        end.map.with_index { |link, i| subc.count == i + 1 ? link  : link += ', '.html_safe }
      end
      row :image
      row :created_at
      row :updated_at
      row :slug
      row :in_development
    end
  end

  filter :title
  filter :created_at

  form do |f|
    f.inputs 'Category details' do
      f.input :title, input_html: { class: 'simple-input' }
      f.input :image
      f.input :slug,  input_html: { class: 'simple-input' }
      f.input :display_priority
      f.input :in_development
    end
    f.actions
  end


  controller do
    def authenticate_user!
      super if params[:action] != 'index'
    end
  end

end
