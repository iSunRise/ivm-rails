ActiveAdmin.register Subcategory do

  menu priority: 4, parent: "Каталог"

  permit_params :title, :image, :category_id

  before_filter :only => [:show, :edit, :update, :destroy] do
    @subcategory = Subcategory.friendly.find(params[:id])
  end

  index do
    selectable_column
    id_column
    column :title
    column :category
    column (:image){ |category| image_tag(category.image_url, style: 'width: 100px') if category.image.present? }
    actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :category
      row 'Мебель в данной категории' do
        fur = Furniture.where(subcategory_id: resource.id)
        # add a links to furniture with ', ' separation if this is not last item
        fur.map do |furniture|
          link_to furniture.title, admin_furniture_path(furniture.id)
        end.map.with_index { |link, i| fur.count == i + 1 ? link  : link += ', '.html_safe }
      end
      row 'Наши работы в данной категории' do
        fur = Portfolio.where(subcategory_id: resource.id)
        # add a links to furniture with ', ' separation if this is not last item
        fur.map do |portfolio|
          link_to portfolio.title, admin_furniture_path(portfolio.id)
        end.map.with_index { |link, i| fur.count == i + 1 ? link  : link += ', '.html_safe }
      end
      row :description
      row :image
      row :created_at
      row :updated_at
      row :slug
    end
  end

  filter :title
  filter :category
  filter :created_at

  form html: { multipart: true }  do |f|
    f.inputs  do
      f.input :category, include_blank: false
      f.input :title, input_html: { class: 'simple-input' }
      f.input :image, as: :file
    end
    f.actions
  end

  controller do
    def authenticate_user!
      super if params[:action] != 'index'
    end
  end

end
