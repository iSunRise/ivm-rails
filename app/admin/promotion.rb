ActiveAdmin.register Promotion do

  menu label: "Акції"

  permit_params :image, :category_id, :description

  index do
    selectable_column
    column ('Зображення')        { |ad| image_tag(ad.image_url, style: 'max-width: 100px') }
    column 'Опис',               :description
    column 'Належить категорії', :category
    column 'Створено',           :created_at
    column 'Оновлено',           :updated_at
    actions
  end

  filter :category
  filter :description

  form do |f|
    f.inputs 'Promotion details' do
      f.input :image, as: :file
      # select categories without promotion
      # and (for edit page) remove current promotion from not allowed ids
      f.input :category,    collection: Category.where.not(id: Promotion.all.map(&:category_id) - [object.category_id])
      f.input :description, input_html: { class: 'simple-input' }
    end
    panel 'Description' do
      "<h4>"\
        "Опис підтримує html-розмітку, і має стандартний розмір шрифту в 25рх.<br>"\
        "Заготовлені різні розміри шрифту для наступних елементів:<br>"\
        "&lt;h1&gt; - 220%<br>"\
        "&lt;h2&gt; - 200%<br>"\
        "&lt;h3&gt; - 160%<br>"\
        "&lt;h4&gt; - 140%<br>"\
        "&lt;h5&gt; - 120%<br>"\
        "&lt;h6&gt; - 100%<br>"\
        "&lt;p&gt; - 80%<br>"\
        "&lt;small&gt; - 60%<br>"\
        "Підтримується необмежена вкладеність"\
      "</h4>".html_safe
    end
    f.actions
  end

end