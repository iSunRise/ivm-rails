# ActiveAdmin.register Translation do
#   permit_params :language, :content
#
#   index do
#     selectable_column
#     id_column
#     column :key
#     column (:language){ |t| LanguageList::LanguageInfo.find(t.language).name }
#     column (:content){ |t| truncate(t.content.to_s, length: 200) }
#     column :updated_at
#     actions
#   end
#
#   filter :content
#   filter :created_at
#
#   form do |f|
#     f.inputs "Translation Details" do
#       f.input :key, input_html: { disabled: '' }
#       f.input :language, as: :select,
#                          collection: LanguageList::COMMON_LANGUAGES.map{|l| [l.name, l.iso_639_1] },
#                          include_blank: false
#       f.input :content
#     end
#     f.actions
#   end
#
# end
