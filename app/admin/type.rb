ActiveAdmin.register Type do

  menu priority: 5, parent: "Каталог"

  permit_params :name, :description, :category_id

  # View
  index do
    selectable_column
    column :id
    column :name
    column :category
    actions
  end

  form do |f|
    f.inputs do
      f.input :category_id, as: :nested_select,
              fields: [:title],
              display_name: :title,
              minimum_input_length: 0,
              level_1: { attribute: :category_id }
      f.input :name, input_html: { class: 'simple-input' }, include_blank: false
      f.input :description, input_html: { class: 'simple-input' }
    end
    f.actions
  end

end
