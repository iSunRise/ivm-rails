ActiveAdmin.register Review do

  # allows_multi_upload(mounted_uploader: :image)

  menu priority: 8, label: "Відгуки"

  permit_params :message, :avatar, :name, :displayable, :category_id, :subcategory_id, :portfolio_id

  filter :created_at, label: 'Створено'

  # Index
  index do
    column 'Имя', :name
    column 'Отображение', :displayable
    actions
  end

  # View
  show do
    attributes_table do
      row :name
      row :message
      row :displayable
      row (:avatar) { |x|
        if x.avatar.size == 0
          'none'.html_safe
        else
          html = '<img src="' + x.avatar_url(:thumb) + '">'
          html.html_safe
        end
      }
      row ('Зображення') { |x|
        next '' if x.images.size == 0
        html = '<img src="'
        html += x.images.map { |p| p.url(:small) }
                  .join('">&nbsp;&nbsp;&nbsp;<img src="')
        html += '">'
        # html += x.images.map {|p| p.image_url(:medium) }
        #                   .join('">&nbsp;&nbsp;&nbsp;<img src="')
        # html += '">'
        html.html_safe
      }
    end
  end

  # Edit
  form do |f|
    f.inputs 'Деталі' do
      f.input :name, input_html: { class: 'simple-input' }, include_blank: false

      f.input :message, input_html: { class: 'simple-input' }, include_blank: false
      f.input :portfolio_id, as: :nested_select,
              fields: [:title],
              display_name: :title,
              minimum_input_length: 0,
              level_1: { attribute: :category_id    },
              level_2: { attribute: :subcategory_id },
              level_3: { attribute: :portfolio_id }
      f.input :avatar
      f.input :displayable
      # f.input :images, input_html: { class: 'simple-input' }
    end
    f.actions
  end
end
