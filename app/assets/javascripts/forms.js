jQuery(document).ready(function(){

  var successMessages = {
    common:      'ВАША ЗАЯВКА ОТПРАВЛЕНА!\nМы свяжемся с Вами в ближайшее время.',
    callback:    'ВАШ ЗАПРОС НА ОБРАТНЫЙ ЗВОНОК\nОТПРАВЛЕН!\nМы свяжемся с Вами в ближайшее время.',
    metering:    'ВАША ЗАЯВКА НА ПРОСЧЕТ ОТПРАВЛЕНА!\nМы свяжемся с Вами в ближайшее время.',
    calculation: 'ВАША ЗАЯВКА НА ЗАМЕР ОТПРАВЛЕНА!\nМы свяжемся с Вами в ближайшее время.',
    application: 'ВАША ЗАЯВКА НА УЧАСТИЕ В АКЦИИ ОТПРАВЛЕНА!\nМы свяжемся с Вами в ближайшее время.'
  };

  $('.js-common-request').submit(function () {
    var formData = new FormData(this);
    submitForm(formData, successMessages.common);
    return false;
  });

  $('.js-request-callback').submit(function () {
    var formData = new FormData(this);
    submitForm(formData, successMessages.callback);
    return false;
  });

  $('.js-request-metering').submit(function () {
    var formData = new FormData(this);
    submitForm(formData, successMessages.metering);
    return false;
  });


  $('.js-request-calculation').submit(function ( ) {
    var formData = new FormData(this);
    submitForm(formData, successMessages.calculation);
    return false;
  });



  $('.js-send-application').submit(function () {
    var formData = new FormData(this);
    submitForm(formData, successMessages.application);
    return false;
  });

  $('.js-request-callback-from-project').submit(function () {
    var formData = new FormData(this);
    submitForm(formData, successMessages.callback);
    return false;
  });


  $('.js-request-metering-from-project').submit(function () {
    var formData = new FormData(this);
    submitForm(formData, successMessages.metering);
    return false;
  });


  $('.js-request-calculation-from-project').submit(function ( ) {
    var formData = new FormData(this);
    submitForm(formData, successMessages.calculation);
    return false;
  });

  function submitForm(formData, title) {
    $.ajax({
      url: '/submit',
      type: 'POST',
      data: formData,
      processData: false,
      contentType: false,
      success: function(){
        swal({
          title: title,
          type: 'success',
          confirmButtonText: '\u00D7',
          confirmButtonColor: '#fcbc7d'
        });
        $('.modal').modal('hide');
      },
      error: function(){
        swal('Ошибка! Попробуйте ещё раз', '', 'error');
      }
    });
  }

});
