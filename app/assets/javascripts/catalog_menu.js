jQuery(document).ready(function(){

  $('.js-menu-item').mouseover(function(){
    var targetBlock = $(this).attr('data-block');
    $('.kitchen-links').removeClass('visible');
    $('.' + targetBlock).addClass('visible');

    $('.js-menu-item').removeClass('active');
    $(this).addClass('active');
  });
});