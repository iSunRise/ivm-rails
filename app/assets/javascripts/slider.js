$(document).ready(function() {

  $("#slick-demo").slick({
    arrows:     true,
    autoplay:   true,
    lazyLoad:   'progressive',
    prevArrow:  '<div class="circle-left"></div>',
    nextArrow:  '<div class="circle-right"></div>',
    initialSlide: 0,
    slidesToShow: 5,

    responsive: [{
      breakpoint: 1199,
      settings: {
        slidesToShow: 3
      }
    } , {
      breakpoint: 784,
      settings: {
        slidesToShow: 2
      }
    }]

  });

  $("#slick-ourworks").slick({
    arrows: false,
    dots: true
  });

  $('.furniture-prev').click(function(){
    var prevIndex = $(this).attr('data-modal-index');
    var currentIndex = parseInt(prevIndex) + 1;
    $('[data-index="' + currentIndex + '"]').modal('hide');
    $('[data-index="' + prevIndex + '"]').modal('show');
  });

  $('.furniture-next').click(function(){
    var nextIndex = $(this).attr('data-modal-index');
    var currentIndex = parseInt(nextIndex) - 1;
    $('[data-index="' + currentIndex + '"]').modal('hide');
    $('[data-index="' + nextIndex + '"]').modal('show');
  });

  // little hack to make it works
  $('.modal').on('show.bs.modal', function (e) {
    var targetId = '#' + $(e.target).attr('id');
    initializer(targetId);
  });

  $('.modal').on('shown.bs.modal', function (e) {
    var targetId = '#' + $(e.target).attr('id');
    var $slickCarousel  = $(targetId + ' .js-slick-carousel');
    $slickCarousel.resize();
  });

  // destroying slick when modal hides to prevent errors
  // and slimscroll just in case
  $('.modal').on('hidden.bs.modal', function (e) {
    var targetId = '#' + $(e.target).attr('id');
    var $slickCarousel  = $(targetId + ' .js-slick-carousel');
    var $thumbsCarousel = $(targetId + ' .js-thumbs-carousel');

    $slickCarousel.slick('unslick');
    $thumbsCarousel.slimScroll({ destroy: true });
  });



  function initializer(targetId) {
    var $slickCarousel  = $(targetId + ' .js-slick-carousel');
    var $thumbsCarousel = $(targetId + ' .js-thumbs-carousel');

    // initialize slick
    $slickCarousel.slick({
      arrows:     true,
      autoplay:   true,
      lazyLoad:   'progressive',
      prevArrow:  '<div class="circle-left"></div>',
      nextArrow:  '<div class="circle-right"></div>',
      initialSlide: 0
    });

    // initialize slimscroll
    $thumbsCarousel.slimScroll({
      height:    '408px',
      size:      '0px',
      wheelStep:  33
    });

    // connecting buttons to slimscroll
    $(targetId + ' .slimscroll-btn.btn-up').unbind().click( function () {
      $thumbsCarousel.slimScroll({ scrollBy: '-136px' });
    });
    $(targetId + ' .slimscroll-btn.btn-down').unbind().click( function () {
      $thumbsCarousel.slimScroll({ scrollBy: '136px' });
    });

    // jumping to an element by clicking thumbnail
    $slickCarousel.parent().next().find('div.js-jumpTo').each(function () {
      var jump = this.dataset.jumpTo;
      $(this).click(function () {
        $slickCarousel.slick('slickGoTo', jump, false);
      })
    });
  }

  $('[data-toggle="modal-with-slider"]').click(function(){
    var directTargetId = '#lightslider-modal-' + this.dataset['target'];
    console.log(this.dataset['target']);
    $(directTargetId).modal('show');

    // insert id into request modals
    $('#project-id-for-metering').val(this.dataset['target']);
    $('#project-id-for-callback').val(this.dataset['target']);
    $('#project-id-for-calculation').val(this.dataset['target']);
  });


  $(document).on('show.bs.tab turbolinks:load', '.nav-tabs-responsive [data-toggle="tab"]', function(e) {
    var $target = $(e.target);
    var $tabs = $target.closest('.nav-tabs-responsive');
    var $current = $target.closest('li');
    var $parent = $current.closest('li.dropdown');
    $current = $parent.length > 0 ? $parent : $current;
    var $next = $current.next();
    var $prev = $current.prev();
    var updateDropdownMenu = function($el, position){
      $el
      .find('.dropdown-menu')
      .removeClass('pull-xs-left pull-xs-center pull-xs-right')
      .addClass( 'pull-xs-' + position );
    };

    $tabs.find('>li').removeClass('next prev');
    $prev.addClass('prev');
    $next.addClass('next');

    updateDropdownMenu( $prev, 'left' );
    updateDropdownMenu( $current, 'center' );
    updateDropdownMenu( $next, 'right' );
  });

  $("#pop").on("click turbolinks:load", function() {
    $('#imagepreview').attr('src', $('#imageresource').attr('src')); // here asign the image to the modal when the user click the enlarge link
    $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
  });

});
