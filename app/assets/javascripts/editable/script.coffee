jQuery(document).ready ->
  $editables = $('[data-editable]')
  return if $editables.length == 0

  $editables.click ->
    source = $(this).next()
    text = source.html()
    key = $(this).data('editable')
    swal {
      title: 'Edit text'
      text: "<textarea id='edit-text' rows=5>#{text}</textarea>"
      html: 'true'
      animation: 'slide-from-top'
      confirmButtonText: 'Save'
      showCancelButton: true
      closeOnConfirm: false
      showLoaderOnConfirm: true
    }, (isConfirm) ->
      return unless isConfirm
      new_text = $('#edit-text').val()
      if new_text != text
        $.ajax(
          type: 'POST'
          url: '/translations.json'
          data: { text: new_text, key: key }
          success: ->
            console.log('success')
            source.html(new_text)
            swal('Saved', '', 'success')
          error: ->
            console.log('error')
            swal('Could not save', 'Please try again', 'error')
        )
      else
        swal.close()

      return
    return false
