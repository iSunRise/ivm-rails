$(document).ready(function () {
  var $reviewsCarousel = $('#slick-review');

  $reviewsCarousel.slick({
    arrows:     true,
    autoplay:   true,
    autoplaySpeed: 20000,
    lazyLoad:   'progressive',
    swipe: false,
    prevArrow:  '<div class="circle-left"></div>',
    nextArrow:  '<div class="circle-right"></div>'
  });
});