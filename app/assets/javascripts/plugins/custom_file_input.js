$(document).ready(function(){
  if($('.js-file-btn').length == 0) { return; }

  $('.js-file-btn').each( function(){
    var $input = $(this),
        $label = $('span.span-target[data-target-for="' + $input.attr('id') + '"]'),
        labelVal = $label.html();

    $input.on( 'change', function( e )
    {
      var fileName = '';

      if (this.files && this.files.length > 1)
        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);

      else if ( e.target.value )
        fileName = e.target.value.split('\\').pop();

      fileName = fileNameParse(fileName);

      if (fileName)
        $label.html('"' + fileName + '"');
      else
        $label.html(labelVal);
    });

    function fileNameParse (fileName) {
      if (fileName.length >= 25) {
        var cuttedFileName = fileName.substring(0, 10) + '...' + fileName.substring(fileName.length - 10, fileName.length);
        return cuttedFileName;
      } else {
        return fileName;
      }
    }
  });

  $('#review_use_catalog_').on( 'change', function () {
    if ($(this).is(':checked')) {
      $field = $('input#review_images');
      $field.prop('disabled', true);
      $field.addClass('disabled');
      $field.next().addClass('disabled');
    }
    else {
      $field = $('input#review_images');
      $field.prop('disabled', false);
      $field.removeClass('disabled');
      $field.next().removeClass('disabled');
    }
  })
});