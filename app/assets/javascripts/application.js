 // This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
// require turbolinks
// require jquery.turbolinks
//= require bootstrap
//= require slider
//= require sweetalert
//= require wow
//= require lightbox
//= require_tree ./plugins/
//= require_tree .
//= require activeadmin_addons/all

$(document).ready(function(){
    $('.phone-user').inputmask({"mask": "+38(099) 999 99 99"});

    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true,
        'showImageNumberLabel': false,
        'disableScrolling' : true,
        'positionFromTop' : 250
    })

    wow = new WOW(
        {
            boxClass:     'wow',      // default
            animateClass: 'animated fadeInDown', // default
            offset:       10,          // default
            mobile:       false,       // default
            live:         false,        // default
            'data-wow-duration': '0.5s'
        }
    );
    wow.init();

});