module EditableHelper

  def editable(key, text = nil, options= {}, &block)
    translation = Translation.find_text(key)
    if translation.present?
      content = translation
    else
      content = text.present? ? text : capture(&block)
    end
    return content.html_safe if current_user.nil? || options[:nowrap]
    ["<span class=\"editable editable-text\">",
     "<a href='#' data-editable='#{key}' class='edit-link'>",
     '<i class="fa fa-edit"></i>',
     '</a>',
     '<span class="editable-content">',
     content,
     '</span></span>'
    ].join.html_safe
  end

  def editable_link(key, text = nil, options= {}, &block)
    content = Translation.find_text(key) || text || capture(&block)
    return content.html_safe if current_user.nil? || options[:nowrap]
    ["<div href='#' data-editable='#{key}' class='edit-link edit-link-left'>",
      '<i class="fa fa-edit"></i>',
      "</div>",
      "<span class=\"editable editable-text\">",
     content,
     '</span>'
    ].join.html_safe
  end

  def editable_image_tag(key, options = {})
    source = Image.find_replacement(key) || key
    return image_tag(source, options) unless current_user.present?
    image_tag(asset_path(source), options.merge('data-editable-image' => key))
  end

end
