class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :give_categories

  def set_error_messages(resource)
    resource.errors.full_messages
  end

  protected

  def give_categories
    @all_categories = Category.all.order(in_development: :asc)
  end
end
