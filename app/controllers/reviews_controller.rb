class ReviewsController < ApplicationController
  def index
    @reviews = Review.where(displayable: true)
  end

  def new
    @review = Review.new
    @categories = Category.all
  end

  def create
    @review = Review.new(review_params)
    @review.images = [] if params[:review][:use_catalog?] == '1' && @review.valid?
    if @review.save
      redirect_to reviews_path
    else
      flash[:danger] = set_error_messages(@review)
      render 'new'
    end
  end

  private

  def review_params
    params.require(:review).permit(:name, :message, :avatar, :portfolio_id, { images: [] })
  end
end