class SubmitsController <  ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :set_cors_headers, only: [:create, :cors]

  def create
    submit = Submit.create(submit_params)

    # emails = EmailRoute.where(site: site_url).map(&:email)
    NotificationMailer.new_request(submit.id).deliver

    render json: { status: :success }
  end

  def cors
    head :ok
  end

  private

  def submit_params
    if params.has_key?(:common)
      common_params = params.require(:common).permit!

      {
        submit_type: :common,
        name:        common_params[:name],
        phone:       common_params[:phone],
        data: {
          message: common_params[:message]
        }
      }
    elsif params.has_key?(:callback)
      callback_params = params.require(:callback).permit!

      {
        submit_type: :callback,
        name:        callback_params[:name],
        phone:       callback_params[:phone],
      }
    elsif params.has_key?(:metering)
      metering_params = params.require(:metering).permit!

      {
        submit_type:  :metering,
        name:         metering_params[:name],
        phone:        metering_params[:phone],
        project_id:   metering_params[:project_id],
        file:         metering_params[:file],

        data: {
          email:   metering_params[:email],
          message: metering_params[:message]
        }
      }
    elsif params.has_key?(:calculation)
      calculation_params = params.require(:calculation).permit!

      {
        submit_type:  :calculation,
        name:         calculation_params[:name],
        phone:        calculation_params[:phone],
        project_id:   calculation_params[:project_id],

        data: {
          address: calculation_params[:address],
          message: calculation_params[:message]
        }
      }
    elsif params.has_key?(:application)
      application_params = params.require(:application).permit!

      {
        submit_type: :application,
        name:        application_params[:name],
        phone:       application_params[:phone],
      }
    else
      {}
    end
  end

  def set_cors_headers
    headers['Access-Control-Allow-Origin']  = request.referer
    headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'X-Requested-With'
    headers['Access-Control-Max-Age']       = '1728000'
  end
end
