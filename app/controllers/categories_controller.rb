class CategoriesController < ApplicationController
  respond_to :html, :json

  def show
    @category = Category.find_by(slug: params[:category_slug])
  end

end
