class SubcategoriesController < ApplicationController

  def show
    @subcategory = Subcategory.find_by(slug: params[:subcategory_slug])
    @furnitures_by_type = {}
    @furnitures_count = 0
    @subcategory.category.types.includes(furnitures: :images).each do |type|
      furnitures = type.furnitures.select { |fu| fu.subcategory_id == @subcategory.id }
      if furnitures.present?
        @furnitures_by_type[type] = furnitures
        @furnitures_count += furnitures.size
      end
    end
  end

end
