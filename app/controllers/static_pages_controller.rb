class StaticPagesController < ApplicationController
  def home
    @categories = Category.where(in_development: false).order(display_priority: :desc).limit(6)
    @last_projects_images = Portfolio.order(creation_date: :desc).map(&:images).flatten! || []
  end

  def kitchens
    @subcategory = Subcategory.all
    @furniture = Furniture.all
  end

  def ourworks
    @portfolios = Portfolio.all
    @show_project = params[:peid].to_i || nil
  end

  def livingbaby

  end

  def officefurniture

  end

  def wardrobes

  end

  def cloakroom

  end

  def hallways

  end
end
