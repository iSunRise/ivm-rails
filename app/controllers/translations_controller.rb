class TranslationsController < ApplicationController

  def create
    text = params[:text]
    key = params[:key]
    t = Translation.find_by(key: key, language: I18n.locale)
    if t.present?
      t.content = text
      t.save
    else
      Translation.create(key: key, language: I18n.locale, content: text)
    end
    render json: {}
  end
end